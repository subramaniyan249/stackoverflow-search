import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StackoverflowServiceService {

  constructor(private http: HttpClient) { }

  public getStackoverflowQuestion(param?: string) {
    return this.http.get<any>(`https://api.stackexchange.com/2.2/search/advanced?order=desc&sort=activity&site=stackoverflow${param}`);
  }

  public getStackoverflowAnswers(param?: string) {
    return this.http.get<any>(`https://api.stackexchange.com/2.2/questions/${param}/answers?order=desc&sort=activity&site=stackoverflow`);
  }
}
