import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterPanelComponent } from '../components/filter-panel/filter-panel.component';
import { QuestionsSectionComponent } from '../components/questions-section/questions-section.component';
import { AnswersComponent} from './answers/answers.component'
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from "@angular/material/form-field";
import {MatSelectModule} from '@angular/material/select';
import {MatPaginatorModule} from '@angular/material/paginator';



@NgModule({
  declarations: [    
    FilterPanelComponent, QuestionsSectionComponent, AnswersComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatPaginatorModule
  ],
  exports: [
    FilterPanelComponent, QuestionsSectionComponent, AnswersComponent
  ]
})

export class StackOverflowModule { }
