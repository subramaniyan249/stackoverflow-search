import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { StackoverflowServiceService } from '../../stackoverflow-service.service';

@Component({
  selector: 'app-questions-section',
  templateUrl: './questions-section.component.html',
  styleUrls: ['./questions-section.component.css'],
})
export class QuestionsSectionComponent implements OnInit {
  @Input() data: Array<any>;
  questionsList: Array<any> = [];
  results: any;

  constructor(
    private router: Router,
    private stackoverflowServiceService: StackoverflowServiceService
  ) {}

  //navigate to the answers page based on the question id
  navigateToAnswer(questions: any): void {
    this.router.navigate(['/answers', questions.question_id], {
      queryParams: { link: questions.link },
      skipLocationChange: false,
    });
  }

  ngOnInit() {
    this.fetachQuestions('');
  }

  //get the questions from the stackoverflow api
  public fetachQuestions(param: any) {
    this.stackoverflowServiceService.getStackoverflowQuestion(param).subscribe(
      (results) => {
        this.results = results;
        this.questionsList = results.items;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  //change the data on filter change
  public onFilterApplied(event: any) {
    this.questionsList = event;
  }

  //get questions based on page change
  public getServerData(event: any) {
    let param = `&page=${event.pageIndex + 1}`;
    this.fetachQuestions(param);
  }
}
