import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { StackoverflowServiceService } from '../../stackoverflow-service.service';

@Component({
  selector: 'app-filter-panel',
  templateUrl: './filter-panel.component.html',
  styleUrls: ['./filter-panel.component.css'],
})


export class FilterPanelComponent implements OnInit {
  filtersForm: FormGroup;
  filterString: string = "";
  selected = 'desc';
  @Output() filteredList: EventEmitter<any> = new EventEmitter();

  constructor(private fb: FormBuilder, private stackoverflowServiceService: StackoverflowServiceService) {}

  ngOnInit(): void {
    this.filtersForm = this.fb.group({
      answers: [''],
      user: [''],
      url: [''],
      title: [''],
      views: [''],
      accepted: ['']
    });
  }

  //group all filters and make a search call with the selected filters
  makeFilterCall() {
    Object.keys(this.filtersForm.controls).forEach(key => {
      let value = this.filtersForm.controls[key].value
      if(value)
      this.filterString = `${this.filterString}&${key}=${value}`
    });
    this.stackoverflowServiceService.getStackoverflowQuestion(this.filterString).subscribe(results => {
      this.filterString = "";
      this.filteredList.emit(results.items);
    },
    error => {
      console.log(error)
    })
  }
}
