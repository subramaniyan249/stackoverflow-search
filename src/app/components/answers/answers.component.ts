import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StackoverflowServiceService } from '../../stackoverflow-service.service';

@Component({
  selector: 'app-answers',
  templateUrl: './answers.component.html',
  styleUrls: ['./answers.component.css']
})
export class AnswersComponent implements OnInit {

  id: any;
  link:any;
  answers_details: any;
  constructor(private _Activatedroute:ActivatedRoute, private stackoverflowServiceService: StackoverflowServiceService) { }


  ngOnInit(): void {
    this.id=this._Activatedroute.snapshot.paramMap.get("id");
    this.link=this._Activatedroute.snapshot.queryParamMap.get("link");
    this.stackoverflowServiceService.getStackoverflowAnswers(this.id).subscribe(results => {
      this.answers_details = results.items;
    },
    error => {
      console.log(error)
    })
  }

}
