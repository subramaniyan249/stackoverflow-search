import { NgModule } from '@angular/core';
import { RouterModule, Routes, RouterLink } from '@angular/router';
import { AppComponent } from './app.component';
import { AnswersComponent } from './components/answers/answers.component';
import { QuestionsSectionComponent} from './components/questions-section/questions-section.component'

const routes: Routes = [
  { path: 'questions', component: QuestionsSectionComponent },
  {
    path: 'answers/:id',
    component: AnswersComponent
  },
  {
    path: '',
    component: QuestionsSectionComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
