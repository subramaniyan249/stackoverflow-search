import { TestBed } from '@angular/core/testing';

import { StackoverflowServiceService } from './stackoverflow-service.service';

describe('StackoverflowServiceService', () => {
  let service: StackoverflowServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StackoverflowServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
