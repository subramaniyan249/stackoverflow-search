import { Component, OnInit } from '@angular/core';
import { StackoverflowServiceService } from './stackoverflow-service.service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  title = 'stack-overflow-search';

  constructor(private stackoverflowServiceService: StackoverflowServiceService) { }


}
